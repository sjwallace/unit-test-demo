import 'whatwg-fetch';
import {
	checkStatus,
	handleError,
	getJSON
} from './fetch-utils';

export default function getEmojis() {
	return fetch('https://api.github.com/emojis', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(checkStatus)
	.then(getJSON)
	.catch(handleError());
}
