import React from 'react';
import ReactDOM from 'react-dom';
import getEmojis from '../promises/promise';

let Example = React.createClass({
 
  propTypes: {
    // This will log a message on the console if 
    // items is not defined or if the wrong type
    // is supplied.
    items: React.PropTypes.array.isRequired,
    // This will only log if the type is wrong.
    prefix: React.PropTypes.string,
  },
  
  // Sane defaults for your component...
  getDefaultProps() {
    return {
      prefix: 'Hello'
    }
  },

  getInitialState() {
    return {
      emojis: null,
      showDefault: false
    }
  },

  componentWillMount() {
    // Here you could setState, fetch data from a server or something else...
    getEmojis()
      .then(data => {
        this.setState({ emojis: data });
      });
  },
  
  componentDidMount() {
    // You now have access to the DOM:
    // console.log(ReactDOM.findDOMNode(this).innerHTML);
    // ... or to component references:
    // console.log(this.refs.foobar.innerHTML);
  },
  
  componentWillUpdate() {
    // console.log('component about to update!');
  },
  
  componentDidUpdate() {
    // console.log('component updated!');
    // DOM is available here...
  },
  
  componentWillUnmount() {
    // Use this to tear down any event listeners
    // or do other cleanup before the compoennt is
    // destroyed.
    // console.log('component will unmount!');
  },
  
  shouldComponentUpdate() {
    // This is called when state/props changed and is 
    // an opportunity where you can return false if
    // you don't want the component to update for
    // some reason.
    return true;
  },
  
  toggle(e) {
    // Invert the chosen default.
    // This will trigger an intelligent re-render of the component.
    this.setState({ showDefault: !this.state.showDefault })
  },

  changeData(e) {
    const data = {
      jasmine: 'https://jasmine.github.io/images/jasmine-horizontal-small.png',
      enzyme: 'https://avatars2.githubusercontent.com/u/698437?v=3&s=60',
      react: 'https://facebook.github.io/react/img/logo.svg'
    };
    this.setState({ emojis: data, changed: true, showDefault: false });
  },
  
  render() {
    const items = this.props.items.map(function (item, index) {
      // Any time you construct a list of elements or components,
      // you need to set the `key` so React can more easily work
      // with the DOM.
      return <li key={index}>{item}</li>;
    });
    const anotherList = this.state.emojis;
    return (
      <div>
        <h2 data-selector="header">{this.props.prefix}</h2>
        <hr />
        <button data-selector="button-toggle" onClick={this.toggle}>Toggle</button>
        {!this.state.changed &&
          <button data-selector="button-change" onClick={this.changeData}>Change Data</button>
        }
        <span data-selector="update" ref="foobar">
          Show default: {this.state.showDefault ? 'True' : 'False'}
        </span>
        <ul data-selector="list">
          {this.state.showDefault && 
            items
          }
          {!this.state.showDefault && this.state.emojis &&
            Object.keys(anotherList).map(function(key, index) {
              return <li key={key}><img src={anotherList[key]} width="60" height="60" /></li>
            })
          }
          {!this.state.showDefault && !this.state.emojis &&
            <p data-selector="loading">Loading ...</p>
          }
        </ul>
    </div>
    );
  }
});

export default Example;
