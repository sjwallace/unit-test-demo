export default (selector, timeout = 500, pollingInterval = 100, cb = () => {}) => {

	let errorInterval = setInterval(() => {
		// if a callback was triggered then fire it on each tick
		cb && typeof cb === 'function' && cb();
	}, pollingInterval);
	// if nothing has been found after a set time then clear the interval
	setTimeout(() => {
		clearInterval(errorInterval);
	}, timeout);
};
