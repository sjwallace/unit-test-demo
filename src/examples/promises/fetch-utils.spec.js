import {
	getResource,
	checkStatus,
	handleError
} from './fetch-utils';
import { __RewireAPI__ as PromiseRewireAPI } from './fetch-utils';

describe('getResource() function output', () => {

	it('should return a string value which maps to the given key parameter', () => {

		let expected = 'sorry there has been a technical error';
		let actual = getResource('technical');
		expect(actual).toEqual(expected);
	});

	it('should return a default string value when the supplied key is not found', () => {

		let expected = 'sorry there has been a technical error';
		let actual = getResource('foo');
		expect(actual).toEqual(expected);
	});

	it('should return a default string value when no parameter is given', () => {

		let expected = 'sorry there has been a technical error';
		let actual = getResource();
		expect(actual).toEqual(expected);
	});
});

describe('handleError() return types', () => {

	let dispatchSpy = jasmine.createSpy('dispatchSpy');

	beforeAll(() => {
		PromiseRewireAPI.__Rewire__('dispatch', dispatchSpy);
	});

	afterEach(() => {
		dispatchSpy.calls.reset();
	});

	afterAll(() => {
		PromiseRewireAPI.__ResetDependency__('dispatch');
	});

	it('should return a function when called with initial errorType', () => {

		let expected = jasmine.any(Function);
		let actual = handleError('foo');
		expect(actual).toEqual(expected);
	});

	it('should dispatch an action when called with an error object parameter', () => {

		let errorType = 'foo';
		let expectedCount = 2;
		let expectedSpyCall = {action: errorType, error: ''};
		handleError(errorType)();

		expect(dispatchSpy.calls.count()).toEqual(expectedCount);
		expect(dispatchSpy).toHaveBeenCalledWith(expectedSpyCall);
	});

	it('should dispatch a default action when called with an error object parameter', () => {

		let errorType = null;
		let expectedCount = 2;
		let expectedSpyCall = {action: 'RESET_APP_STATE_ON_ERROR', error: 'bar'};
		handleError(errorType)({message: 'bar'});

		expect(dispatchSpy.calls.count()).toEqual(expectedCount);
		expect(dispatchSpy).toHaveBeenCalledWith(expectedSpyCall);
	});

	it('should dispatch an action denoting that the call in progress has resolved', () => {

		let expectedSpyCall = {action: 'CALL_IN_PROGRESS', inProgress: false};
		handleError()();
		expect(dispatchSpy).toHaveBeenCalledWith(expectedSpyCall);
	});
});

describe('checkStatus() return types', () => {

	it('should return the response object supplied when the status code is within the bounds and there is not errorCode', () => {

		let expected = {status: 200};
		let actual = checkStatus(expected);
		expect(actual).toEqual(expected);
	});

	it('should return a message on the erorr object when one is thrown', () => {

		let expected = 'sorry there has been a technical error';
		let actual = () => checkStatus({status: 200, errorCode: '000'});

		try {
			actual();
		} catch(e) {
			expect(e.message).toBeDefined();
			expect(e.message).toEqual(expected);
		}
	});

	describe('conditions for throwing an error', () => {

		let expected = new Error(getResource());

		it('should throw and error when no response is supplied', () => {

			let actual = () => checkStatus();
			expect(actual).toThrow(expected);
		});

		it('should throw and error when the status is not within the bounds', () => {

			let actual = () => checkStatus({status: 400});
			expect(actual).toThrow(expected);
		});

		it('should throw and error when a error code is present and the status is within the bounds', () => {

			let actual = () => checkStatus({status: 200, errorCode: '000'});
			expect(actual).toThrow(expected);
		});
	});
});
