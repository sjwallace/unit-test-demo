// check to see if debug is passed in as a flag
var Jasmine = require('jasmine');
var JasmineConsoleReporter = require('jasmine-console-reporter');
var jasmine = new Jasmine();
var cleanup = require('jsdom-global')();

var reporter = new JasmineConsoleReporter({
    colors: 2,           // (0|false)|(1|true)|2
    cleanStack: 1,       // (0|false)|(1|true)|2|3
    verbosity: 4,        // (0|false)|1|2|(3|true)|4
    listStyle: 'indent', // "flat"|"indent"
    activity: true
});

// cleanup jsdom injection
jasmine.onComplete(cleanup);

jasmine.loadConfigFile('spec/support/jasmine.json');
jasmine.addReporter(reporter);
jasmine.execute();
