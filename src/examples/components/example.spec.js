import React from 'react';
import { mount, shallow } from 'enzyme';
import fetchMock from 'fetch-mock';
import getEmojis from '../promises/promise';
import Example from './example';

describe('<Example />', () => {

	let component;
	let spyEvent;
	let props = {
		items: ['Jasmine', 'Enzyme', 'React'],
		prefix: 'Hello'
	};

	function buildComponent(props) {
		return shallow(<Example {...props} />);
	}

	beforeAll(done => {
		fetchMock.get(/.*\/emojis/, 
			{
				'8ball':"https://assets-cdn.github.com/images/icons/emoji/unicode/1f3b1.png?v6",
				'100':"https://assets-cdn.github.com/images/icons/emoji/unicode/1f4af.png?v6",
				'1234':"https://assets-cdn.github.com/images/icons/emoji/unicode/1f522.png?v6"
			});
		getEmojis().then(done);
	});

	afterAll(fetchMock.restore);

	it('renders a component', () => {
		component = buildComponent(props);
		expect(component).toBeDefined();
	});

	it('should render H2 with text from props', () => {
		component = buildComponent(props);
		// console.log(component.find('[data-selector="header"]').html());
		expect(component.find('[data-selector="header"]').length).toEqual(1);
		expect(component.find('[data-selector="header"]').text()).toEqual(props.prefix);
	});

	it('should render Buttons', () => {
		component = buildComponent(props);
		expect(component.find('button').length).toEqual(2);

		component.setState({changed: true});
		expect(component.find('button').length).toEqual(1);
	});

	it('should render Example message with text', () => {
		component = buildComponent(props);
		expect(component.find('[data-selector="update"]').length).toEqual(1);
		expect(component.find('[data-selector="update"]').text()).toEqual('Show default: False');

		component.setState({showDefault: true});
		expect(component.find('[data-selector="update"]').text()).toEqual('Show default: True');
	});

	it('should render Loading message by default', () => {
		component = buildComponent(props);
		expect(component.find('[data-selector="list"]').length).toEqual(1);
		// console.log(component.find('[data-selector="list"]').html());
		expect(component.find('[data-selector="list"]').children().length).toEqual(1);
		expect(component.find('[data-selector="loading"]').length).toEqual(1);
		expect(component.find('[data-selector="loading"]').text()).toEqual('Loading ...');
	});

	it('should render List of items from props', () => {
		component = buildComponent(props);
		component.setState({showDefault: true});
		// console.log(component.find('[data-selector="list"]').html());
		expect(component.find('[data-selector="list"]').children().length).toEqual(props.items.length);

		component.setProps({ items: ['foo'] });
		expect(component.find('[data-selector="list"]').children().length).toEqual(1);
	});

	it('should render List with items from state', () => {
		component = buildComponent(props);
		// console.log(component);
		component.setState({emojis: {
			'foo': 'bar'
		}});
		expect(component.find('[data-selector="list"]').children().length).toEqual(1);
	});

	it('should click on toggle button', () => {
		component = buildComponent(props);
		//console.log(component.state('showDefault'));
		expect(component.state('showDefault')).toEqual(false);

	    component.find('[data-selector="button-toggle"]').simulate('click');
	    expect(component.state('showDefault')).toEqual(true);
	});

	it('should click on change button', () => {
		component = buildComponent(props);
		// console.log(component.state());
		expect(component.state('emojis')).toBeNull();

	    component.find('[data-selector="button-change"]').simulate('click');
	    // console.log(component.state());
	    expect(component.state('emojis')).not.toBeNull();
	    expect(component.state('changed')).toEqual(true);
	});
	
});