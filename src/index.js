import React from 'react';
import ReactDOM from 'react-dom';
import Example from './examples/components/example';

ReactDOM.render(<Example items={['Jasmine', 'Enzyme', 'React']} />, document.getElementById('app'));
