import async from './async';

describe('async() polling', () => {

	beforeAll(() => {
		spyOn(global, 'clearInterval').and.callThrough();
	});

	beforeEach(done => {
		setTimeout(() => {
			done();
		}, 600);
	});

	it('should fire a callback function for each tick of the interval', (done) => {
		let timeout = 500;
		let cb = jasmine.createSpy('callbackSpy');
		let expected = 4;

		async('.error', 500, 100, cb);

		setTimeout(() => {
			expect(cb).toHaveBeenCalled();
			expect(cb.calls.count()).toEqual(expected);
			expect(global.clearInterval).toHaveBeenCalled();
			done();
		}, 600);
	});
});
