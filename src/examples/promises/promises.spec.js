import fetchMock from 'fetch-mock';
import getEmojis from './promise';
import { __RewireAPI__ as PromiseRewireAPI } from './promise';

describe('getEmojis() async response', () => {

	let stub = {hello: 'world'};
	let res;

	it('should make a rest call to a specified endpoint and return a response', () => {
		expect(res).toEqual(stub);
	});
});

describe('getEmojis() checks status of api response', () => {

	let stub = {hello: 'world'};
	let checkStatusSpy;

	beforeAll(done => {
		fetchMock.get(/.*\/emojis/, stub);
		getEmojis().then(done);
	});

	it('should check the status of the fetch response', () => {
		expect(checkStatusSpy.calls.count()).toEqual(1);
	});
});

describe('getEmojis() handling errors', () => {

	let stub = {status: 400};
	let handleErrorSpy;
	let getJsonSpy;

	beforeAll(done => {
		fetchMock.mock(/.*\/emojis/, stub);
	});

	afterEach(() => {
		// perform needed cleanup
	});

	afterAll(fetchMock.restore);

	it('should make a rest call to a specified endpoint and return an error when failed', () => {
		expect(handleErrorSpy).toHaveBeenCalledWith(new Error('sorry there has been a technical error'));
		expect(handleErrorSpy.calls.count()).toEqual(1);
	});

	it('should not call intermediate methods in the promise chain when an error occurs', () => {
		expect(getJsonSpy).not.toHaveBeenCalled();
	});
});
