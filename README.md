# Unit Test Demo

To run the unit tests

```
npm test
```

To start the app

```
npm start
```

NOTE: there are failing unit tests...this is intentional as thats the exercise :-)