function dispatch(payload = {}) {
	return payload;
}

function getResource(resourceType) {
	let errors = {
		technical: 'sorry there has been a technical error'
	};
	return resourceType && errors[resourceType] || errors.technical;
}

function checkStatus(response = {}) {
	// if our response is valid then simply return the response
	if (response.status >= 200 && response.status < 300 && !response.errorCode) {
		return response;
	}
	// if we have encountered an error status or there is an errorCode
	// then attempt to fetch the error object pertaining to that code.
	let errorMessage = getResource(response.errorCode);
	// throw an error here to be caught by our promise chain's catch
	// and add the error object details along with the error thrown
	let error = new Error(errorMessage);
	error.message = errorMessage;
	throw error;
}

function handleError(errorType) {

	return (error = {}) => {
		// tell the application that an error has occurred
		dispatch({
			action: errorType || 'RESET_APP_STATE_ON_ERROR',
			error: error.message || ''
		});
		// update rest call status
		dispatch({
			action: 'CALL_IN_PROGRESS',
			inProgress: false
		});
	};
}

function getJSON(response) {
	return response.json();
}

export {
	dispatch,
	getResource,
	checkStatus,
	handleError,
	getJSON
};
